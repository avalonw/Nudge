# Nudge

A little script I use to remind myself that people who I don't see every day still exist and that I'd like to talk to them.
*note, I run OpenBSD as my main machine, so this uses their version of 'date' and I have no idea if it will work on other systems as is*

Nudge checks through plain text files in a given folder. This folder is specified in the variable base_folder at the top of the script.
For nudge to work as intended, each text file should be named for the person it represents. eg. Sam_Person (nudge doesn't currently support filenames with spaces, I recommend using underscores) and must include the lines "Nudge_Date: YYYY-MM-DD" and "Frequency: X" where the date on the Nudge_Date line is when you would like to next contact the person, and the X after Frequency: is how many days you'd like between each nudge.

Provided is an example of this folder filled with functional files, named examplenudgefolder. Both the piglet and winnie-the-pooh have additionally a Notes field, it is recommended that this field stays at the bottom of the file. As is visible in piglet's file, it is not necessary for the Nudge_Date and/or Frequency to be in a particular location in the file, so long as they are each a single line with the correct format. piglet also has an email address listed, which would be copied to the clipboard if -m was used.

When nudge is run, it compares the Nudge_Date in each file to the current date. For every file in which the Nudge_Date is before or on the current date, nudge will display in reverse chronological order, the Nudge_Date and the name of the file (and thus of the person). So, the bottom of the list is the person who has been awaiting a nudge the longest. If nudge returns nothing at all, then there is no one in your folder you need to contact today. 

## Options:

-c file		nudge will move the Nudge_Date in that file forward to the current date plus the number of days indicated by the Frequency in the file. This option can also be used to add new files, as if the filename isn't recognized, it will ask the user if a new file should be created, and in how many days the user would like to be nudged about this person. It will then add a Nudge_Date that many days into the future, and a Frequency of that many days to the newly created file.  

-e file		nudge will open the file in designated $EDITOR. NOTE: Fragile, no default currently set. I expect that if $EDITOR isn't set on a machine, this will fail.

-m file		nudge will copy email address if listed as "email: example@place.whatever" in nudge file to the clipboard (this option is a preliminary stand-in for the 'Preferred_Contact_Mode' behaviors as discribed in the roadmap) 

## Roadmap

Currently not in any specific order, a rough list of features I intend (or am considering [??]) to add.

- extend to be sxmo pmos compatible
- create a "Prefered_Contact_Mode:" field and corresponding id fields, build an option to drop the user into a conversation in that mode
	- email (neomutt) 
	- text (sxmo_modemtext.sh if on sxmo, else "text this number 12345678" )
	- phone call (sxmo_modemdial.sh if on sxmo, else "call this number 12345678"
	- fedi (toot ?)
	- matrix (need to find a client)
- build and share implementations of useful hooks (sendhooks to -c a given file in mutt, or sxmo ect.)
- ?? a "Reminder:" field as a specific temperary note for next contact, to be displayed with the normal nudge command
- ?? a "notes" option that returns everything from the "Notes:" indicator to the end of the file
- update "edit" option to include a default if $EDITOR is not set. 

